/**
�������� ����� Animal � ��������� ������� Voice(), ������� ������� � ������� ������ � �������. 
���������� �� Animal ������� ��� ������ (� ������� Dog, Cat � �. �.) � � ��� ����������� ����� Voice() ����� �������, 
����� ��� ������� � ������ Dog ����� Voice() ���������� Woof! � �������. 
� ������� main �������� ������ ���������� ���� Animal � ��������� ���� ������ ��������� ��������� �������. 
����� ���������� ������ �� �������, ������� �� ������ �������� ������� ����� Voice(). 
������������� ��� ������. ������ ���������� ��������� �� ����� �������-����������� Animal.
*/

#include <iostream>
#include <string.h>
using namespace std;

class Animal
{
public:
	Animal()
	{}
	
	virtual void Voice()
	{
		cout << "I am the Animal" << endl;
	}
};

class Wolf: public Animal
{
public:
	Wolf()
	{}

	void Voice() override
	{
		cout << "I am the Wolf" << endl;
	}
};

class Spider : public Animal
{
public:
	Spider()
	{}

	void Voice() override
	{
		cout << "I am the Spider" << endl;
	}
};

class Myncroft_player : public Animal
{
public:
	Myncroft_player()
	{}

	void Voice() override
	{
		cout << "I am the Myncroft_player" << endl;
	}
};

int main()
{
	Animal* Animals[3];
	Animals[0] = new Wolf();
	Animals[1] = new Spider();
	Animals[2] = new Myncroft_player();

	for (int i = 0; i < 3; i++)
	{
		Animals[i]->Voice();
	}

	for (int i = 0; i < 3; i++)
	{
		delete Animals[i];
	}

	return 0;
}
#include "helpers.h"

double quadeSum(double a, double b)
{
	double res = (a + b) * (a + b);
	return res;
}
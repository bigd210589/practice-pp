/**
�������� �����, ������ �������� ����� ��� ������ � ���������� ��������� ���� ������� �����. 
������� � ������������, ������� ������� �� ����� ��������, � �������� ������������ ������ ������������ �������. 
�������� �� ������������ ����� ������� � ��������� ��� ���� � ��������� � �������.
������������ ������ �� �������� ���������� �����, ��������� ��������.
�������� ��� ����� � ���� ������� � ��������������� ����.
*/
/*
#include <iostream>
#include <string.h>
using namespace std;

class  Player
{
public:
	Player() : name("Nameless"), point(0)
	{}

	Player(string _name, double _point) : name(_name), point(_point)
	{}

	string getName()
	{
		return name;
	}

	void setName(const string &nevName)
	{
		name = nevName;
	}

	double getPoint()
	{
		return point;
	}

	void setPoint(const double &newPoint)
	{
		point = newPoint;
	}

private:
	string name;
	double point;
};

// ���������� ����� (�� ��������� - ������ ������� ������ �� �� ����� �����)
void shellSort(Player playerlist[], int lengthLisnt)
{
	for (int step = lengthLisnt / 2; step > 0; step /= 2)
	{
		for (int i = step; i < lengthLisnt; i++)
		{
			int j = i;
			while (j >= step && playerlist[j - step].getPoint() > playerlist[i].getPoint())
			{
				swap(playerlist[j], playerlist[j - step]);
				j -= step;
			}
		}
	}
}

// ����������� ����������
void bubbleSort(Player playerlist[], int lengthLisnt)
{
	while (lengthLisnt--)
	{
		bool swepped = false;
		for (int i = 0; i < lengthLisnt; i++)
		{
			if (playerlist[i].getPoint() < playerlist[i + 1].getPoint())
			{
				swap(playerlist[i], playerlist[i + 1]);
				swepped = true;
			}
		}
		if (swepped == false)
		{
			break;
		}
	}
}

int main()
{
	int length;
	cout << "Enter the number of players. =>";
	cin >> length;
	Player* players = new Player[length];

	for (int i = 0; i < length; i++)
	{
		string name;
		double points;
		cout << "Enter the player's name: ";
		cin >> name;
		cout << "Enter the number of points: ";
		cin >> points;

		players[i].setName(name);
		players[i].setPoint(points);
	}

	bubbleSort(players, length);

	for (int i = 0; i < length; i++)
	{
		cout << players[i].getName() << " Point = " << players[i].getPoint() << endl;
	}

	delete[] players;

	return 0;
}
*/